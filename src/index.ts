// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

import * as http from 'http';
import express from 'express';
import promClient from 'prom-client';

const app = express();

const dailyActiveE2EERoomsMetric = new promClient.Gauge({
    name: 'synstats_daily_active_e2ee_rooms',
    help: 'Daily active E2EE rooms'
});

const dailyActiveRoomsMetric = new promClient.Gauge({
    name: 'synstats_daily_active_rooms',
    help: 'Daily active rooms'
});

const dailyActiveUsersMetric = new promClient.Gauge({
    name: 'synstats_daily_active_users',
    help: 'Daily active users'
});

const dailyE2EEMessagesMetric = new promClient.Gauge({
    name: 'synstats_daily_e2ee_messages',
    help: 'Daily E2EE messages'
});

const dailyMessagesMetric = new promClient.Gauge({
    name: 'synstats_daily_messages',
    help: 'Daily messages'
});

const dailySentE2EEMessagesMetric = new promClient.Gauge({
    name: 'synstats_daily_sent_e2ee_messages',
    help: 'Daily sent E2EE messages'
});

const dailySentMessagesMetric = new promClient.Gauge({
    name: 'synstats_daily_sent_messages',
    help: 'Daily sent messages'
});

const dailyUserTypeBridgedMetric = new promClient.Gauge({
    name: 'synstats_daily_user_type_bridged',
    help: 'Daily user type bridged'
});

const dailyUserTypeGuestMetric = new promClient.Gauge({
    name: 'synstats_daily_user_type_guest',
    help: 'Daily user type guest'
});

const dailyUserTypeNativeMetric = new promClient.Gauge({
    name: 'synstats_daily_user_type_native',
    help: 'Daily user type native'
});

const monthlyActiveUsersMetric = new promClient.Gauge({
    name: 'synstats_monthly_active_users',
    help: 'Monthly active users'
});

const r30v2UsersAllMetric = new promClient.Gauge({
    name: 'synstats_r30v2_users_all',
    help: 'R30v2 users all'
});

const r30v2UsersAndroidMetric = new promClient.Gauge({
    name: 'synstats_r30v2_users_android',
    help: 'R30v2 users Android'
});

const r30v2UsersElectronMetric = new promClient.Gauge({
    name: 'synstats_r30v2_users_electron',
    help: 'R30v2 users Electron'
});

const r30v2UsersIOSMetric = new promClient.Gauge({
    name: 'synstats_r30v2_users_ios',
    help: 'R30v2 users iOS'
});

const r30v2UsersWebMetric = new promClient.Gauge({
    name: 'synstats_r30v2_users_web',
    help: 'R30v2 users Web'
});

const totalNonbridgedUsersMetric = new promClient.Gauge({
    name: 'synstats_total_nonbridged_users',
    help: 'Total nonbridged users'
});

const totalRoomCountMetric = new promClient.Gauge({
    name: 'synstats_total_room_count',
    help: 'Total room count'
});

const totalUsersMetric = new promClient.Gauge({
    name: 'synstats_total_users',
    help: 'Total users'
});


function updateMetrics(metrics: any) {
    setMetricIfDefined(dailyActiveE2EERoomsMetric, metrics?.daily_active_e2ee_rooms);
    setMetricIfDefined(dailyActiveRoomsMetric, metrics?.daily_active_rooms);
    setMetricIfDefined(dailyActiveUsersMetric, metrics?.daily_active_users);
    setMetricIfDefined(dailyE2EEMessagesMetric, metrics?.daily_e2ee_messages);
    setMetricIfDefined(dailyMessagesMetric, metrics?.daily_messages);
    setMetricIfDefined(dailySentE2EEMessagesMetric, metrics?.daily_sent_e2ee_messages);
    setMetricIfDefined(dailySentMessagesMetric, metrics?.daily_sent_messages);
    setMetricIfDefined(dailyUserTypeBridgedMetric, metrics?.daily_user_type_bridged);
    setMetricIfDefined(dailyUserTypeGuestMetric, metrics?.daily_user_type_guest);
    setMetricIfDefined(dailyUserTypeNativeMetric, metrics?.daily_user_type_native);
    setMetricIfDefined(monthlyActiveUsersMetric, metrics?.monthly_active_users);
    setMetricIfDefined(r30v2UsersAllMetric, metrics?.r30v2_users_all);
    setMetricIfDefined(r30v2UsersAndroidMetric, metrics?.r30v2_users_android);
    setMetricIfDefined(r30v2UsersElectronMetric, metrics?.r30v2_users_electron);
    setMetricIfDefined(r30v2UsersIOSMetric, metrics?.r30v2_users_ios);
    setMetricIfDefined(r30v2UsersWebMetric, metrics?.r30v2_users_web);
    setMetricIfDefined(totalNonbridgedUsersMetric, metrics?.total_nonbridged_users);
    setMetricIfDefined(totalRoomCountMetric, metrics?.total_room_count);
    setMetricIfDefined(totalUsersMetric, metrics?.total_users);
}

function setMetricIfDefined(metric: promClient.Gauge, value: any) {
    if (value !== undefined) {
        metric.set(convertToNumber(value));
    }
}

function convertToNumber(value: any): number {
    if (typeof value === 'number') {
        return value;
    } else if (typeof value === 'string') {
        const numericValue = parseFloat(value);
        return isNaN(numericValue) ? 0 : numericValue;
    } else {
        return 0;
    }
}

app.use(express.json());

app.put('/', (req: express.Request, res: express.Response) => {
    const data = req.body;

    const currentDate = new Date().toLocaleString();
    console.log(`Current date: ${currentDate}`);
    console.log(data);

    updateMetrics(data);

    res.status(200).send('Data received and cached!');
});

app.get('/metrics', async (req: express.Request, res: express.Response) => {
    try {
        res.set('Content-Type', promClient.register.contentType);
        res.send(await promClient.register.metrics());
    } catch (ex) {
        res.status(500).end(ex);
    }
});

const port = process.env.PORT || 3000;
const server = http.createServer(app);

server.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});
