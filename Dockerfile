# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

FROM node:lts AS builder

WORKDIR /app
COPY . .

ENV NODE_ENV=development

RUN npm ci --only=prod --prefer-offline --progress=false --no-audit \
    && npm install --only=dev --prefer-offline --progress=false --no-audit \
    && npm run build \
    && npm prune production

FROM node:lts

WORKDIR /app

COPY --from=builder /app/dist /app
COPY --from=builder /app/node_modules /app/node_modules
 
ENV NODE_ENV=production 

CMD [ "node", "index.js" ]