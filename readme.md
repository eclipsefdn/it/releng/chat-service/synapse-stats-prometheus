<!--
SPDX-FileCopyrightText: 2024 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# synapse stats prometheus exporter

Allows you to convert the json format sent by synapse into gauge format for prometheus.

## Run

```shell
npm run build && npm start
```

## Test


```shell
curl -X PUT -H "Content-Type: application/json" -d '{ "total_users": 181 }' http://localhost:3000/

curl http://localhost:3000/metrics
```